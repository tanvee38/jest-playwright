module.exports = {
  // setupFilesAfterEnv: ['./jest.setup.js'],
  setupFilesAfterEnv: ["jest-allure/dist/setup"],
  verbose: true,
  bail: false,
  reporters: ["default", "jest-allure"]
}