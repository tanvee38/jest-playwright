jest.setTimeout(30000);

// for jest allure example kindly see: https://github.com/zaqqaz/jest-allure/blob/master/examples/__tests__/test1.js

const { Severity } = require("jest-allure/dist/Reporter");

const playwright = require('playwright');

describe(`UI Tests with Playwright`, () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await playwright[process.env.BROWSER].launch({ headless: false});

    page = await browser.newPage();
  
    await page.goto('https://www.athabascau.ca/');
  })
  
  test('sample test 1 with allure report', async () => {
    reporter
      .description("Home Page test suite")
      .story("GOOGL-01")
      .severity(Severity.Critical)
      .testId('TEST-01');

      reporter.startStep("Check the title");
      const title = await page.$('#content-title');
  
    const titleText = await page.evaluate(title => title.textContent, title);
    
    expect(titleText).toBe("How AU Works Athabasca University");
    reporter.endStep();

    const howAUWorksLink = await page.$('.link-block.local-link');

    await Promise.all([
      page.waitForNavigation(),
      page.evaluate(howAUWorksLink => howAUWorksLink.click(), howAUWorksLink)
    ]);

    const formSubmitButton = await page.$('#submit');

    const formSubmitButtonText = await page.evaluate(formSubmitButton => formSubmitButton.textContent, formSubmitButton);

    expect(formSubmitButtonText).toBe("Submit");
  })

  test.skip('sample test 2 with allure report', async () => {
    const contentTitle = await page.$('#content-title');
  
    const contentTitleText = await page.evaluate(contentTitle => contentTitle.textContent, contentTitle);

    expect(contentTitleText).toBe('How AU Works');
  })

  afterAll(async () => {
    await browser.close();
  });
})